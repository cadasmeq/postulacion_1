# Desarrollo: PRUEBA TÉCNICA

Esta prueba tiene como objetivo medir el uso de tecnologías y cómo aplicarlas a un problema en particular, en este caso, el desarrollo de un Modulo con sus funciones básicas de Ingreso, Actualización, Eliminación y Consulta.

La medición de esta prueba será en base a la capacidad de resolución del problema como un todo, ya sea en el modelado de datos como en la funcionalidad del Modulo, en caso de no saber cómo desarrollar una, se valora la capacidad de investigación.

en la sección de `docs/` se puede encontrar mayor detalle sobre la implementación y funcionamiento del presente proyecto.

## Casos de Uso

- [x] Ingreso de producto
- [x] Edición de producto
- [x] Eliminación de producto
- [x] Ingreso de Bodega
- [x] Edición de Bodega
- [x] Eliminación de Bodega
- [x] Ingreso de stock en una Bodega en particular
- [x] Listado de productos, todos los artículos o un articulo en particular indicando stock y detalle del articulo, en bodega.

## Indicaciones Técnicas y Entregables: 
### Lenguaje 
- Backend: Python (deseable Versión 3.x), Django (2.x)
- Frontend:   Opcional, HTML, JavaScript (ambiente Web)
### BBDD
- PostgreSQL o MySQL

### Entregables 
- [x] Modelo de datos
- [x] Archivo Dump o Backup de base de datos para ser restaurado y validar trabajo (Indicar si es Postgres o Mysql)
- [x] Código fuente (Documentar código, explicación breve de que hace el código)
- [x] Descripción de librerías en caso de utilizar alguna en particular.

### Validaciones
La validación del código sera levantado y validado en el siguiente ambiente:
- Apache
- Python (deseable Versión 3.x)
- Django (2.x)
- Base de Datos según base de datos utilizada (Enviar todo lo necesario para levantar aplicativo).
