#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


# Make initial migrations
python manage.py makemigrations
python manage.py migrate

# Load initial data to test
python manage.py loaddata initial_products
python manage.py loaddata initial_inventories
python manage.py loaddata initial_stockrooms

python manage.py runserver 0.0.0.0:8000