# Django
runserver:
	python3 manage.py runserver

makemig:
	python3 manage.py makemigrations

migrate:
	python3 manage.py migrate

shell_plus:
	python3 manage.py shell_plus

# Docker
rebuild:
	docker-compose down --remove-orphans
	docker-compose build --no-cache

reset:
	docker-compose down --volumes --remove-orphans
	docker-compose pull

# Development
prepare-env:
	bash scripts/install_dev_dependencies.sh

install-precommit:
	pre-commit install

flake8:
	flake8 src/ tests/

isort:
	isort src/ tests/

black:
	black src/ tests/

pytest:
	pytest tests/
