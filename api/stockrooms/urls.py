from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views.inventory_crud_view import InventoryViewSet
from .views.stockrooms_crud_view import StockRoomsViewSet

router = DefaultRouter()
router.register(r"stockrooms", StockRoomsViewSet, basename="stockrooms")
router.register(r"inventories", InventoryViewSet, basename="inventories")


urlpatterns = [
    path("", include(router.urls)),
]
