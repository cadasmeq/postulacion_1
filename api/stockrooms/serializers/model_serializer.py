"""Inventory and StockRooms serializers."""

from rest_framework import serializers

from api.stockrooms.models import Inventory, StockRoom


class StockRoomModelSerializer(serializers.ModelSerializer):
    """StockRoom Model Serializer"""

    class Meta:
        """Meta class."""

        model = StockRoom
        fields = "__all__"


class InventoryModelSerializer(serializers.ModelSerializer):
    """Inventory Model Serializer"""

    class Meta:
        """Meta class."""

        model = Inventory
        fields = "__all__"
