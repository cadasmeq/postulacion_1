"""Inventories models"""

from django.db import models

from api.utils.models import APIModel


class Inventory(APIModel):
    """Django Inventory Model"""

    product = models.ForeignKey("products.Product", on_delete=models.CASCADE)
    stock = models.SmallIntegerField("product stock number")
