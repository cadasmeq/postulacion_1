"""StockRoom Model"""

from django.db import models

from api.utils.models import APIModel


class StockRoom(APIModel):
    """Django StockRoom Model"""

    inventories = models.ManyToManyField("stockrooms.Inventory")

    name = models.CharField("stockrooms name", max_length=140, blank=True)
    description = models.TextField("stoockroom description", blank=True)

    def __str__(self):
        """Return stockrooms name."""
        return self.name

    class Meta:
        """Meta class."""

        ordering = ["-name"]
