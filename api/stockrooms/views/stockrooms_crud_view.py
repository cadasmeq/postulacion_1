from rest_framework import mixins, viewsets

from api.stockrooms.models import StockRoom
from api.stockrooms.serializers.model_serializer import StockRoomModelSerializer


class StockRoomsViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """StockRoom view set."""

    serializer_class = StockRoomModelSerializer
    lookup_field = "id"

    def get_queryset(self):
        """Restrict list to public-only."""
        queryset = StockRoom.objects.all()
        if self.action == "list":
            return queryset.filter(is_active=True)
        return queryset
