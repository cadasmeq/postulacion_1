from rest_framework import mixins, viewsets

from api.stockrooms.models import Inventory
from api.stockrooms.serializers.model_serializer import InventoryModelSerializer


class InventoryViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """Inventory view set."""

    serializer_class = InventoryModelSerializer
    lookup_field = "id"

    def get_queryset(self):
        """Restrict list to public-only."""
        queryset = Inventory.objects.all()
        if self.action == "list":
            return queryset.filter(is_active=True)
        return queryset
