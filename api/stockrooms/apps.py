"""Stockrooms app."""

from django.apps import AppConfig


class StockRoomsAppConfig(AppConfig):
    """Stockrooms app config."""

    name = "api.stockrooms"
    verbose_name = "StockRooms"
