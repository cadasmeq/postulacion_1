"""User model."""

from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db import models

from api.utils.models import APIModel


class User(APIModel, AbstractUser):
    """User model.
    Extend from Django's Abstract User, change the username field
    to email and add some extra fields.
    """

    email = models.EmailField(
        "email address",
        unique=True,
        error_messages={"unique": "A user with that email already exists."},
    )

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username"]

    is_client = models.BooleanField(
        "client", default=True, help_text=("Clients are the main type of user.")
    )

    def __str__(self):
        """Return username."""
        return self.username
