"""Users app."""

from django.apps import AppConfig


class UsersAppConfig(AppConfig):
    """Users app config."""

    name = "api.users"
    verbose_name = "Users"
