"""Global Urls"""

from django.urls import include, path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from api.dummy.views import DummyView

urlpatterns = [
    path("dummy/", DummyView().as_view(), name="dummy"),
    path("api/token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("api/token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    path("", include(("api.products.urls", "products"), namespace="products")),
    path("", include(("api.stockrooms.urls", "stockrooms"), namespace="stockrooms")),
]
