"""Product Model"""

from django.db import models

from api.utils.models import APIModel


class Product(APIModel):
    """Django Product Model"""

    class Sizes(models.TextChoices):
        LARGE = "L", ("LARGE")
        MEDIUM = "M", ("MEDIUM")
        SMALL = "S", ("SMALL")

    name = models.CharField("product name", max_length=140)
    description = models.TextField(blank=True)
    comments = models.TextField(blank=True)
    size = models.CharField(
        "product size",
        max_length=1,
        choices=Sizes.choices,
        blank=True,
    )
    weight = models.FloatField(
        "product weight",
        blank=True,
        null=True,
    )
    price = models.FloatField(
        "product price",
        blank=True,
        null=True,
    )

    def __str__(self):
        """Return Product name."""
        return self.name

    class Meta:
        """Meta class."""

        ordering = ["-name", "-id"]
