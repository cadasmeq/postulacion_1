"""Product serializers."""

from rest_framework import serializers

from api.products.models import Product


class ProductModelSerializer(serializers.ModelSerializer):
    """Product Model Serializer"""

    class Meta:
        """Meta class."""

        model = Product
        fields = "__all__"
