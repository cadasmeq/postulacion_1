from .model_serializer import ProductModelSerializer

__all__ = [
    "ProductModelSerializer",
]
