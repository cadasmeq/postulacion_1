from rest_framework import mixins, viewsets

from api.products.models import Product
from api.products.serializers import ProductModelSerializer


class ProductViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """Product view set."""

    serializer_class = ProductModelSerializer
    lookup_field = "id"

    def get_queryset(self):
        """Restrict list to public-only."""
        queryset = Product.objects.all()
        if self.action == "list":
            return queryset.filter(is_active=True)
        return queryset
