"""Product app."""

from django.apps import AppConfig


class ProductAppConfig(AppConfig):
    """Product app config."""

    name = "api.products"
    verbose_name = "Products"
