# Generated by Django 4.0.1 on 2022-01-20 15:51

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Product",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "created",
                    models.DateTimeField(
                        auto_now_add=True,
                        help_text="Date time on which the object was created.",
                        verbose_name="created at",
                    ),
                ),
                (
                    "modified",
                    models.DateTimeField(
                        auto_now=True,
                        help_text="Date time on which the object was last modified.",
                        verbose_name="modified at",
                    ),
                ),
                (
                    "is_active",
                    models.BooleanField(default=True, verbose_name="is active"),
                ),
                ("name", models.CharField(max_length=140, verbose_name="product name")),
                ("description", models.TextField(blank=True)),
                ("comments", models.TextField(blank=True)),
                (
                    "size",
                    models.CharField(
                        blank=True,
                        choices=[("L", "LARGE"), ("M", "MEDIUM"), ("S", "SMALL")],
                        max_length=1,
                        verbose_name="product size",
                    ),
                ),
                (
                    "weight",
                    models.FloatField(
                        blank=True, null=True, verbose_name="product weight"
                    ),
                ),
                (
                    "price",
                    models.FloatField(
                        blank=True, null=True, verbose_name="product price"
                    ),
                ),
            ],
            options={
                "ordering": ["-name", "-id"],
            },
        ),
    ]
