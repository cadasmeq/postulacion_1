from rest_framework.response import Response
from rest_framework.views import APIView


class DummyView(APIView):
    """DummyView always return ok"""

    def get(self, request):
        """Returns ok"""
        return Response("ok", status=200)
