"""Dummy app."""

from django.apps import AppConfig


class DummyAppConfig(AppConfig):
    """Dummy app config."""

    name = "api.dummy"
    verbose_name = "Dummy"
