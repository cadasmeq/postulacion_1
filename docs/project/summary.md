# About the Project
## How to Run
This project can be launch using `docker-compose up` command. This will run a new django server adding some records at the Database that can be used to check its features.

For more information about this containerized service, check `docker-compose.yml` file.

## Database Engine
It was chosen `SQLite3` as database engine in order to avoid waste time in complex implementation to store in external infraestructure (Postgres, RDS, etc).

## Django API
It were chosen Django Viewsets to create CRUD Endpoints of each model in order to guarantee a fast implementation pointing to fulfill requirements of this challenge. 

*Personal Thoughts: Those three endpoints might be absolutly redesign into a much better implementation that point into a single endpoint that simplify the CRUD actions. I decided to avoid this in order seize the time of this challenge, but its completly a much better idea to do in a long term*


## How to Test API Endpoints
Every endpoint and its http methods are listed in `docs/api/` folder. Once project is up, you can use those references to test manually every endpoint or you can import `API-Requests-Collection.json` in your favorite API Client (ThunderClient, Postman, etc) and have the collection used to develop this project.

## About Model Design
It was desgin three models that can satisfy every point detailed in README.md. Those models are nexts:

- Product       : Define a base product model structure
- Inventory     : Data Model that allows gather product and its stock number
- StockRoom     : Physical place where are stored every inventory objects.

The file `models diagram.drawio` represents its model relationship, this file can be upload into a diagram software to have access to its content.

## Other Features
Some of the features that you can find here are nexts:
- Gitlab Integration (testing and lintern).
- Pre-commits hooks for developers.
- Makefile as entrypoint to interact with the project.
- Tox integration (pytest and flake8)
- Own Python Package (empty implementation that can be used in the whole project)
