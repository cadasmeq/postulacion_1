# Project Structure

## Folders & Files
### Docker:
    - .docker/      : Contains django base docker image
    - .envs/        : Stores .envs files splitted by services.
    - .dbs/         : Stores a backup of the database.
    - requirements/ : Stores base, develop and production dependecies using differents files for it.

### Django (API)
    - tests/        : Folder that contains every test that project requires.
    - docs/         : Contains documentation about this project, decision taken trough each new develop
    - api/          : Contains API which django exposes
    - src/          : Internal package that can be used to segregate code dependencies and api/ could use (is required install it manually by now [pip install -e .])

### Repository Settings Files
    - .pre-commit-config.yml  : pre-commit config file used to run certains commands that will run before every new git commit
    - .gitlab-ci.yml          : Gitlab CI/CD yaml
    - docker-compose.yml      : Declares every services and how its containers must be initialized.
    - Makefile                : Project entrypoint that can be used to run some useful commands

# Packages
## Django
- Django                : Web server library
- Django Rest Framework : Django Rest Framework library
- Django-environ        : Django library to access a environ variables
- Django-extensions     : Utilities for Django
- Django-cors-headers   : CORS for Django
- SimpleJWT             : Django implementation for Json Web Token
- psycopg               : Postgres integration

## Development
- flake8        : Python lint
- tox           : Simple (but slow) way to run testing suite
- pytest        : Python testing package
- pytest        : Pytest coverage complementary package
- pytest-django : Pytest utilities for django

## Pre Commit
pre-commit      : Allow run certains commands before to each git commit

## Code Formatting
black           : Automatic python code formatting
isort           : Automatic python ordering file imports
