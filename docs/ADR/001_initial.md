# 1. Record Architecture Decisions
Date: 20-01-2021

## Status
Accepted

## Context
Job Application project, it is required that architecure be simplest as possible, so it was chosen a Monolith Architecture to resolve what is mentionated in README file

## Decision
For all architectural decision, it will be created Architectural Decision Log (ADL). 
All decisions will be recorded as Architecture Decision Records (ADR).
Each ADR will be recorded using [Michael Nygard template](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions), which contains following sections: Status, Context, Decision and Consequences.

## Consequences
All architectural decisions should be recorded in log. Old decisions should be recorded as well with an approximate decision date. New decisions should be recorded on a regular basis.