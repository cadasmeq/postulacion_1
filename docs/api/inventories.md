# Inventory Endpoint

Base inventory endpoints is a viewset that acts as a CRUD endpoint
that allows to Retrieve, List, Update and Create only active inventory.

```
    "GET: {
        "endpoint": "inventories/",
        "status_code": 200,
        "description": "Retrieves complete list of inventories."
    },
    "GET: {
        "endpoint": "inventories/<inventory_id: int>",
        "status_code": 200,
        "description": "Retrieves a single inventory"
    },
    "POST": {
        "endpoint": "inventories/",
        "status_code": 201,
        "payload": {
            "format": "JSON",
            "required": [
                <product_id: int>,
                <stock: 500>,
            ]
        },
        "description": "Creates a new inventory"
    }
    "PATCH": {
        "endpoint": "inventories/<inventory_id: int>",
        "status_code": 200,
        "payload": {
            "format": "JSON",
            "fields": [
                "stock",
            ]
        },
        "description": "Partial update an existent inventory"
    },
```
