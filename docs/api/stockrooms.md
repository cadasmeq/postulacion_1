# stockrooms Endpoint

Base stockrooms endpoints is a viewset that acts as a CRUD endpoint
that allows to Retrieve, List, Update and Create only active stockrooms.

```
    "GET: {
        "endpoint": "stockrooms/",
        "status_code": 200,
        "description": "Retrieves complete list of stockrooms."
    },
    "GET: {
        "endpoint": "stockrooms/<stockrooms_id: int>",
        "status_code": 200,
        "description": "Retrieves a single stockrooms"
    },
    "POST": {
        "endpoint": "stockrooms/",
        "status_code": 201,
        "payload": {
            "format": "JSON",
            "required": [
                <slug_name: str>,
                <inventories: int>
            ]
        },
        "description": "Creates a new stockroom"
    }
    "PATCH": {
        "endpoint": "stockrooms/<stockrooms_id: int>",
        "status_code": 200,
        "payload": {
            "format": "JSON",
            "fields": [
                "description",
                "size",
                "price",
            ]
        },
        "description": "Partial update an existent stockrooms"
    },
```
