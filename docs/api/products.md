# Products Endpoint

Base products endpoints is a viewset that acts as a CRUD endpoint
that allows to Retrieve, List, Update and Create only active products.

```
    "GET: {
        "endpoint": "products/",
        "status_code": 200,
        "description": "Retrieves complete list of products."
    },
    "GET: {
        "endpoint": "products/<product_id: int>",
        "status_code": 200,
        "description": "Retrieves a single products"
    },
    "POST": {
        "endpoint": "products/",
        "status_code": 201,
        "payload": {
            "format": "JSON",
            "required": ["name"]
        },
        "description": "Creates a new product"
    }
    "PATCH": {
        "endpoint": "products/<product_id: int>",
        "status_code": 200,
        "payload": {
            "format": "JSON",
            "fields": [
                "name",
                "description",
                "price",
                ...
            ]
        },
        "description": "Partial update a new product"
    },
```
