# Dummy Endpoint
Dummy endpoints allows to test servers availability returning always an "ok" response with 200 status code.

```
    "GET: {
        "endpoint": "dummy/",
        "status_code": 200,
        "description": "Returns ok"
    }
```
