import pytest
from django.conf import settings
from django.db import transaction

from api.products.models import Product
from api.stockrooms.models import Inventory, StockRoom


@pytest.fixture(scope="session")
def setup_test_db():
    settings.DATABASES["default"].update({"NAME": ".dbs/test.db"})


@pytest.fixture
def new_product():
    """Creates a dummy product register in database"""
    return Product.objects.create(name="dummy")


@pytest.fixture
def new_inventory(new_product):
    """Creates a dummy inventory register in database"""
    with transaction.atomic():
        inv = Inventory.objects.create(
            stock=50,
            product=new_product,
        )
    inv.save()
    return inv


@pytest.fixture
def new_stockroom(new_inventory):
    """Creates a dummy stockroom register in database"""
    with transaction.atomic():
        stockroom = StockRoom.objects.create(
            name="dummy",
            description="Dummy StockRoom",
        )
        stockroom.inventories.add(new_inventory)
        stockroom.save()
    return stockroom
