import pytest
from django.urls import reverse


@pytest.mark.django_db
@pytest.mark.parametrize(
    "url, expected_url",
    [
        ("dummy", "/dummy/"),
    ],
)
def test_api_urls(url, expected_url, client):
    """Test every api urls"""
    url = reverse(url)
    assert url == expected_url


@pytest.mark.django_db
@pytest.mark.parametrize(
    "url, expected_response",
    [
        ("dummy", "ok"),
    ],
)
def test_api_responses(url, expected_response, client):
    url = reverse("dummy")
    response = client.get(url)
    assert response.status_code == 200
    assert response.data == expected_response
