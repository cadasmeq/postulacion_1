"""Test StockRooms Viewset"""

import pytest
from black import json
from django.urls import reverse

from api.stockrooms.models import StockRoom

STOCKROOM_DETAILS = [
    ("La Florida", "Bodega La Florida, Santiago"),  # name and description pair
    ("Manquehue", "Bodega Manquehue, Santiago"),  # name and description pair
]


@pytest.mark.django_db
@pytest.mark.parametrize("name,description", STOCKROOM_DETAILS)
def test_endpoint_successfuly_create_stockroom(
    name, description, client, new_inventory
):
    """Test creation stockroom endpoint"""
    new_inventory

    url = reverse("stockrooms:stockrooms-list")
    response = client.post(
        url,
        data={
            "name": name,
            "description": description,
            "inventories": 1,
        },
    )

    assert response.status_code == 201
    assert response.data["name"] == name
    assert response.data["description"] == description
    assert StockRoom.objects.count() == 1


@pytest.mark.django_db
def test_endpoint_successfuly_list_stockroom(client, new_stockroom):
    """Test endpoint that list all stockrooms"""
    new_stockroom

    url = reverse("stockrooms:stockrooms-list")

    response = client.get(url)
    assert response.status_code == 200
    assert len(response.data) == StockRoom.objects.count()


@pytest.mark.django_db
@pytest.mark.parametrize(
    "status_code,data",
    [
        (
            200,
            {"description": "This is a dummy description"},
        )
    ],
)
def test_endpoint_successfuly_update_stockroom(
    status_code, data, client, new_stockroom
):
    """Test endpoint that list all stockrooms"""
    new_stockroom  # add a new stockroom

    response = client.patch(
        "/stockrooms/1/",
        data=json.dumps(data),
        content_type="application/json",
    )
    assert response.status_code == status_code
    assert response.data["description"] == data["description"]


@pytest.mark.django_db
def test_endpoint_successfuly_retrieve_stockroom(client, new_stockroom):
    """Test endpoint that can retrieve an specific stockrooms by its id"""
    room = new_stockroom

    response = client.get("/stockrooms/1/")
    assert response.status_code == 200
    assert response.data["name"] == room.name
    assert response.data["description"] == room.description


@pytest.mark.django_db
def test_endpoint_successfuly_delete_stockroom(client, new_stockroom):
    """Test endpoint that can delete an specific stockroom by its id"""
    new_stockroom

    response = client.delete("/stockrooms/1/")
    assert response.status_code == 204

    response = client.get("/stockrooms/1/")
    assert response.status_code == 404
