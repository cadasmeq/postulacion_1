"""Test Inventory"""

import json

import pytest
from django.urls import reverse

from api.stockrooms.models import Inventory


@pytest.mark.django_db
@pytest.mark.parametrize("stock,product_id", [(100, 1)])
def test_endpoint_successfuly_create_inventory(stock, product_id, client, new_product):
    """Test endpoint that can create a new inventory"""
    new_product  # creates a new product

    url = reverse("stockrooms:inventories-list")
    response = client.post(
        url,
        data={"stock": stock, "product": product_id},
    )

    assert response.status_code == 201
    assert response.data["stock"] == stock
    assert response.data["product"] == product_id
    assert Inventory.objects.count() == 1


@pytest.mark.django_db
def test_endpoint_successfuly_list_inventories(client, new_inventory):
    """Test endpoint that list all inventories"""
    new_inventory  # creates a new inventory

    url = reverse("stockrooms:inventories-list")
    response = client.get(url)

    assert response.status_code == 200
    assert len(response.data) == Inventory.objects.count()


@pytest.mark.django_db
@pytest.mark.parametrize(
    "status_code,data",
    [
        (200, {"stock": 5}),
        (200, {"stock": 10}),
        (200, {"stock": 15}),
    ],
)
def test_endpoint_successfuly_update_inventories(
    status_code, data, client, new_inventory
):
    """Test endpoint that list all inventories"""
    new_inventory  # creates a new inventory

    response = client.patch(
        "/inventories/1/",
        data=json.dumps(data),
        content_type="application/json",
    )
    assert response.status_code == status_code
    assert response.data["stock"] == data["stock"]


@pytest.mark.django_db
def test_endpoint_successfuly_retrieve_inventory(client, new_inventory):
    """Test endpoint that can retrieve an specific inventory by its id"""
    inventory = new_inventory

    response = client.get("/inventories/1/")
    assert response.status_code == 200
    assert response.data["stock"] == inventory.stock


@pytest.mark.django_db
def test_endpoint_successfuly_delete_inventory(client, new_inventory):
    """Test endpoint that can delete an specific inventory by its id"""
    new_inventory  # creates a new inventory
    response = client.delete("/inventories/1/")
    assert response.status_code == 204

    response = client.get("/inventories/1/")
    assert response.status_code == 404
