"""Test Products Viewset"""


import pytest
from black import json
from django.urls import reverse

from api.products.models import Product

PRODUCTS_LIST = [
    ("TV"),
    ("Laptop"),
    ("Camera"),
    ("Jeans"),
]


def new_product():
    prod = Product.objects.create(name="dummy")
    return prod


@pytest.mark.django_db
@pytest.mark.parametrize("name,", PRODUCTS_LIST)
def test_endpoint_successfuly_create_products(name, client):
    """Test creation products endpoint"""

    url = reverse("products:products-list")

    response = client.post(url, data={"name": name})
    assert response.status_code == 201
    assert response.data["name"] == name
    assert Product.objects.count() == 1


@pytest.mark.django_db
@pytest.mark.parametrize("name", PRODUCTS_LIST)
def test_endpoint_successfuly_list_products(name, client):
    """Test endpoint that list all products"""
    total_products = 5

    for i in range(total_products):
        _ = new_product()

    url = reverse("products:products-list")

    response = client.get(url)
    assert response.status_code == 200
    assert len(response.data) == total_products
    assert len(response.data) == Product.objects.count()


@pytest.mark.django_db
@pytest.mark.parametrize(
    "status_code, data",
    [
        (
            200,
            {
                "description": "This is a dummy description",
                "price": 2000.0,
                "weight": 10.0,
                "size": "M",
            },
        )
    ],
)
def test_endpoint_successfuly_update_products(status_code, data, client):
    """Test endpoint that list all products"""
    _ = new_product()

    response = client.patch(
        "/products/1/",
        data=json.dumps(data),
        content_type="application/json",
    )
    assert response.status_code == status_code
    assert response.data["description"] == data["description"]
    assert response.data["price"] == data["price"]
    assert response.data["weight"] == data["weight"]
    assert response.data["size"] == data["size"]


@pytest.mark.django_db
def test_endpoint_successfuly_retrieve_products(client):
    """Test endpoint that can retrieve an specific product by its id"""
    product = new_product()

    response = client.get("/products/1/")
    assert response.status_code == 200
    assert response.data["name"] == product.name


@pytest.mark.django_db
def test_endpoint_successfuly_delete_products(client):
    """Test endpoint that can delete an specific product by its id"""
    _ = new_product()
    response = client.delete("/products/1/")
    assert response.status_code == 204

    response = client.get("/products/1/")
    assert response.status_code == 404
